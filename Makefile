BUSYBOX:= build/busybox

.PHONY: FORCE

initrd: $(BUSYBOX) Makefile init
	rm -rf build/initrd-root || true
	mkdir -p build/initrd-root
	cd build/initrd-root && mkdir -p bin
	cp -Lr $(BUSYBOX) build/initrd-root/bin
	cp init build/initrd-root/init
	cd build/initrd-root/bin && ./busybox --list | \
		while read name; do \
			ln -s busybox $$name; \
		done
	(cd build/initrd-root && find | cpio -H newc -ov) > $@.tmp
	mv $@.tmp $@

bysybox $(BUSYBOX):
	mkdir -p build
	wget https://www.busybox.net/downloads/binaries/1.31.0-defconfig-multiarch-musl/busybox-x86_64 -O $@.tmp
	mv $@.tmp $@
	chmod +x $@
